package landmark.assignment.carousel;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import landmark.assignment.dataprovider.datamodel.Product;
import landmark.assignment.dataprovider.datamodel.ProductData;
import landmark.assignment.uiutil.TypeFaceUtil;
import lm.assignment.R;

/**
 * Created by Binil on 24/09/2016.
 */
public class CarouselView {

    private ProductView mProductLeft;
    private ProductView mProductRight;
    private static int sWidth = 0;
    private Context mContext;

    public CarouselView(View mainContent) {
        mContext = mainContent.getContext();

        //finding the width of the image view used in product,as incoming image is too small
        int screenWidth = mContext.getResources().getDisplayMetrics().widthPixels;
        sWidth = (screenWidth / 2) - (mainContent.getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin) +
                mainContent.getResources().getDimensionPixelOffset(R.dimen.carosine_elemnt_margin));
        mProductLeft = new ProductView(mainContent.findViewById(R.id.fr_carousel_left_product));
        mProductRight = new ProductView(mainContent.findViewById(R.id.fr_carousel_right_product));
    }

    /**
     * update the data when response or scrolling
     *
     * @param productData
     */
    public void setProductData(ProductData productData) {
        //first we set the left side elements
        setProductInfo(productData.getProductLeft(), mProductLeft);
        setProductInfo(productData.getProductRight(), mProductRight);
    }

    /**
     * set the two products inside
     *
     * @param product
     * @param productView
     */
    private void setProductInfo(Product product, ProductView productView) {
        Glide.with(mContext)
                .load(product.getImageUrl())
                .placeholder(R.mipmap.ic_launcher)
                .fitCenter()
                .crossFade()
                .into(productView.mProductImageView);

        productView.mBrandTextView.setText(product.getTitle());
        productView.mProductName.setText(product.getProductName());
        productView.mPriceTextView.setText(product.getCurrentPrice());

        //set strike out text with old price
        boolean hasOldPrice = !(product.getActualPrice() == null || product.getActualPrice().isEmpty());
        productView.mPriceTextViewStrikeOut.setVisibility(hasOldPrice ? View.VISIBLE : View.GONE);
        productView.mPriceTextViewStrikeOut.setText(product.getActualPrice());

        //set left side offer text
        boolean hasLeftLabel = !(product.getLeftCornerText() == null || product.getLeftCornerText().trim().isEmpty());
        productView.mNewLabelText.setVisibility(hasLeftLabel ? View.VISIBLE : View.GONE);
        productView.mNewLabelText.setText(product.getLeftCornerText());

        //set left side offer text
        boolean hasRightLabel = !(product.getRightCornerText() == null || product.getRightCornerText().trim().isEmpty());
        productView.mOfferLabelText.setVisibility(hasRightLabel ? View.VISIBLE : View.GONE);
        productView.mOfferLabelText.setText(product.getRightCornerText());
    }

    private class ProductView {
        private ImageView mProductImageView;
        private TextView mBrandTextView;
        private TextView mProductName;
        private TextView mPriceTextView;
        private TextView mPriceTextViewStrikeOut;
        private RelativeLayout mImageContainer;
        private TextView mNewLabelText;
        private TextView mOfferLabelText;

        public ProductView(View view) {
            mImageContainer = (RelativeLayout) view.findViewById(R.id.carousel_cell_product_image_layout);
            mProductImageView = (ImageView) view.findViewById(R.id.carousel_cell_product_image);
            mBrandTextView = (TextView) view.findViewById(R.id.carousel_cell_brand_name);
            mProductName = (TextView) view.findViewById(R.id.carousel_cell_product_name);
            mPriceTextView = (TextView) view.findViewById(R.id.carousel_cell_price);
            mNewLabelText = (TextView) view.findViewById(R.id.carousel_cell_new_label);
            mOfferLabelText = (TextView) view.findViewById(R.id.carousel_cell_offer_label);
            mPriceTextViewStrikeOut = (TextView) view.findViewById(R.id.carousel_cell_price_strike);

            TypeFaceUtil.setTypeFace(mBrandTextView, view.getResources().getAssets(), TypeFaceUtil.FontType.LATO_MEDIUM);
            TypeFaceUtil.setTypeFace(mNewLabelText, view.getResources().getAssets(), TypeFaceUtil.FontType.LATO_MEDIUM);
            TypeFaceUtil.setTypeFace(mOfferLabelText, view.getResources().getAssets(), TypeFaceUtil.FontType.LATO_MEDIUM);
            TypeFaceUtil.setTypeFace(mPriceTextView, view.getResources().getAssets(), TypeFaceUtil.FontType.LATO_BOLD);
            TypeFaceUtil.setTypeFace(mPriceTextViewStrikeOut, view.getResources().getAssets(), TypeFaceUtil.FontType.LATO_THIN);
            TypeFaceUtil.setTypeFace(mProductName, view.getResources().getAssets(), TypeFaceUtil.FontType.LATO_THIN);
            mPriceTextViewStrikeOut.setPaintFlags(mPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            setHeight();
        }

        public void setHeight() {
            if (sWidth > 0) {
                mProductImageView.setLayoutParams(new RelativeLayout.LayoutParams(sWidth, sWidth));
                mImageContainer.setLayoutParams(new LinearLayout.LayoutParams(sWidth, sWidth));
                mNewLabelText.setMaxWidth(sWidth / 2);
                mOfferLabelText.setMaxWidth(sWidth / 2);
            }
        }
    }
}

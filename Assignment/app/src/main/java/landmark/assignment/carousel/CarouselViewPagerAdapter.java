package landmark.assignment.carousel;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import landmark.assignment.dataprovider.datamodel.ProductData;
import lm.assignment.R;

/**
 * Created by Binil on 24/09/2016.
 */
public class CarouselViewPagerAdapter extends PagerAdapter {

    private ArrayList<ProductData> mProductData;

    public CarouselViewPagerAdapter(ArrayList<ProductData> productData) {
        mProductData = productData;
    }

    public void updateDataSet(ArrayList<ProductData> productData) {
        mProductData = productData;
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        View mainContent = LayoutInflater.from(collection.getContext()).inflate(R.layout.fragment_carousel, null);
        collection.addView(mainContent);
        CarouselView carouselView = new CarouselView(mainContent);
        carouselView.setProductData(mProductData.get(position));
        return mainContent;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mProductData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}

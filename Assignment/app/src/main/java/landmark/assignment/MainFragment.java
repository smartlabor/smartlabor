package landmark.assignment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import landmark.assignment.dataprovider.ApiCallBack;
import landmark.assignment.dataprovider.DataProvider;
import landmark.assignment.dataprovider.datamodel.HomePageData;
import lm.assignment.R;

/**
 * Created by Binil on 24/09/2016.
 */
public class MainFragment extends Fragment implements ApiCallBack {

    private RecyclerView mRecycleView;
    private HomePageData mHomePageData;

    public String getFragmentDefaultTag() {
        return this.getClass().getSimpleName();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new DataProvider(getActivity(), getLoaderManager(), this).provideData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainContent = inflater.inflate(R.layout.fragment_main_n, null);
        mRecycleView = (RecyclerView) mainContent.findViewById(R.id.main_fr_recycle);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return mainContent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mHomePageData != null) {
            this.onResponseReceived(mHomePageData);
        }
    }

    @Override
    public void onResponseReceived(HomePageData homeData) {
        mHomePageData = homeData;
        if (getActivity() != null) {
            ((MainActivity) getActivity()).hideProgress();
        }
        if (mRecycleView != null) {
            mRecycleView.setAdapter(new MainAdapter(homeData.getProductBlockData()));
        }
    }

    @Override
    public void onError(Exception es) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).showError(es.getMessage());
            ((MainActivity) getActivity()).hideProgress();
        }
    }
}

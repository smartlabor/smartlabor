package landmark.assignment.dataprovider.datamodel;

import java.util.ArrayList;

/**
 * Created by Binil on 25/09/2016.
 */
public class HomePageData {

    private Exception mException;
    private ArrayList<ProductBlockData> mProductBlockData = new ArrayList<>();

    public void setProductBlockData(ProductBlockData blockData) {
        mProductBlockData.add(blockData);
    }

    public ArrayList<ProductBlockData> getProductBlockData() {
        return mProductBlockData;
    }

    public void setError(Exception e) {
        mException = e;
    }

    public Exception getError() {
        return mException;
    }

    public boolean hasError() {
        return mException != null;
    }
}

package landmark.assignment.dataprovider.datamodel;

/**
 * Created by Binil on 25/09/2016.
 */
public class Product {

    private String mTitle;
    private String mProductName;
    private String mCurrentPrice;
    private String mActualPrice;
    private String mRightCornerText;
    private String mLeftCornerText;
    private String mImageUrl;

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String mProductName) {
        this.mProductName = mProductName;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getCurrentPrice() {
        return mCurrentPrice;
    }

    public void setCurrentPrice(String mCurrentPrice) {
        this.mCurrentPrice = mCurrentPrice;
    }

    public String getActualPrice() {
        return mActualPrice;
    }

    public void setActualPrice(String mActualPrice) {
        this.mActualPrice = mActualPrice;
    }

    public String getRightCornerText() {
        return mRightCornerText;
    }

    public void setRightCornerText(String mRightCornerText) {
        this.mRightCornerText = mRightCornerText;
    }

    public String getLeftCornerText() {
        return mLeftCornerText;
    }

    public void setLeftCornerText(String mLeftCornerText) {
        this.mLeftCornerText = mLeftCornerText;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }
}

package landmark.assignment.dataprovider.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Binil on 25/09/2016.
 */
public class CacheDb {

    public static final String TABLE_PRODUCTS = "Products";
    public static final String TABLE_TYPE = "ProductType";

    // Common column names
    private static final String KEY_ID = "id";
    public static final String COLUMN_PRODUCT_TYPE = "product_type";

    // NOTES Table - column nmaes
    public static final String COLUMN_PRODUCT_TITLE = "product_title";
    public static final String COLUMN_PRODUCT_NAME = "product_name";
    public static final String COLUMN_PRODUCT_PRICE_NOW = "product_price_now";
    public static final String COLUMN_PRODUCT_PRICE_OLD = "product_price_old";
    public static final String COLUMN_PRODUCT_RIGHT_TEXT = "product_right_corner_text";
    public static final String COLUMN_PRODUCT_LEFT_TEXT = "product_leftt_corner_text";
    public static final String COLUMN_PRODUCT_IMAGE = "product_image_url";

    private static CacheDb mCacheDb;
    private DatabaseHelper databaseHelper;

    private CacheDb(Context context) {
        databaseHelper = new DatabaseHelper(context);

    }

    public static CacheDb getInstance(Context context) {
        if (mCacheDb == null) {
            mCacheDb = new CacheDb(context);
        }
        return mCacheDb;
    }

    /**
     * insert the values to data base
     *
     * @param tableName     table name
     * @param contentValues values to be inserted
     */
    synchronized public void insert(String tableName, ContentValues contentValues) {
        if (databaseHelper != null) {
            SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();
            sqLiteDatabase.insert(tableName, null, contentValues);
        }
    }

    public Cursor query(String tableName, String selection, String[] selectionArgs) {
        if (databaseHelper != null) {
            SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();
            Cursor cursor = sqLiteDatabase.query(tableName,
                    null,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    null);
            return cursor;
        }
        return null;
    }

    synchronized public void updateCacheEntity(String tableName, ContentValues contentValues, String whereClause, String[] args) {
        if (databaseHelper != null) {
            SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();
            sqLiteDatabase.update(tableName, contentValues, whereClause,
                    args);
        }
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "LandMarkDb";

        // Table Create Statements
        // Todo table create statement
        private static final String CREATE_TABLE_PRODUCT_TYPE = "CREATE TABLE "
                + TABLE_TYPE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + COLUMN_PRODUCT_TYPE
                + " TEXT )";

        // Tag table create statement
        private static final String CREATE_TABLE_PRODUCTS = "CREATE TABLE " + TABLE_PRODUCTS
                + "(" + KEY_ID + " INTEGER PRIMARY KEY," + COLUMN_PRODUCT_TITLE + " TEXT,"
                + COLUMN_PRODUCT_NAME + " TEXT,"
                + COLUMN_PRODUCT_PRICE_OLD + " TEXT,"
                + COLUMN_PRODUCT_PRICE_NOW + " TEXT,"
                + COLUMN_PRODUCT_RIGHT_TEXT + " TEXT,"
                + COLUMN_PRODUCT_LEFT_TEXT + " TEXT,"
                + COLUMN_PRODUCT_IMAGE + " TEXT," + ")";

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            // creating required tables
            db.execSQL(CREATE_TABLE_PRODUCT_TYPE);
            db.execSQL(CREATE_TABLE_PRODUCTS);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // on upgrade drop older tables
            db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_PRODUCT_TYPE);
            db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_PRODUCTS);

            // create new tables
            onCreate(db);
        }
    }
}

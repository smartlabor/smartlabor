package landmark.assignment.dataprovider.datamodel;

import java.util.ArrayList;

/**
 * Created by Binil on 25/09/2016.
 */
public class ProductBlockData {


    private String mMainTitle;
    private ArrayList<ProductData> mProductData = new ArrayList<>();

    public void setProductTitle(String title) {
        mMainTitle = title;
    }

    public String getProductTitle() {
        return mMainTitle;
    }

    public void setProductArray(ArrayList<ProductData> productData) {
        mProductData = productData;
    }

    public ArrayList<ProductData> getProductArray() {
        return mProductData;
    }

    public void addProductData(ProductData productData) {
        mProductData.add(productData);
    }

}

package landmark.assignment.dataprovider;

import landmark.assignment.dataprovider.datamodel.HomePageData;

/**
 * Created by Binil on 25/09/2016.
 */
public interface ApiCallBack {

    void onResponseReceived(HomePageData homeData);

    void onError(Exception es);
}

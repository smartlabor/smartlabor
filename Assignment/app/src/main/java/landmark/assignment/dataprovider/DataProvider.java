package landmark.assignment.dataprovider;

import android.content.Context;
import android.support.v4.app.LoaderManager;

import landmark.assignment.dataprovider.datamodel.HomePageData;

/**
 * Created by Binil on 25/09/2016.
 */
public class DataProvider implements ApiCallBack {

    private static final int LOADER_ID = 99;
    private ApiCallBack mApiCallBack;
    private LoaderManager mLoaderManager;
    private LoaderCallbacks mCallbacks;

    public DataProvider(Context context, LoaderManager loaderManager, ApiCallBack apiCallBack) {
        this.mLoaderManager = loaderManager;
        mCallbacks = new LoaderCallbacks(context, this);
        mApiCallBack = apiCallBack;
    }

    public void provideData() {
        mLoaderManager.restartLoader(LOADER_ID, null, mCallbacks);
    }


    @Override
    public void onResponseReceived(HomePageData homeData) {
        mLoaderManager.destroyLoader(LOADER_ID);
        if (mApiCallBack != null) {
            mApiCallBack.onResponseReceived(homeData);
        }
    }

    @Override
    public void onError(Exception es) {
        if (mApiCallBack != null) {
            mApiCallBack.onError(es);
        }
    }
}

package landmark.assignment.dataprovider;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import landmark.assignment.dataprovider.datamodel.HomePageData;

/**
 * Created by Binil on 25/09/2016.
 */
public class LoaderCallbacks implements LoaderManager.LoaderCallbacks<HomePageData> {

    private Context mContext;
    private ApiCallBack mApiCallBack;

    public LoaderCallbacks(Context context, ApiCallBack apiCallBack) {
        mContext = context;
        mApiCallBack = apiCallBack;
    }

    @Override
    public Loader<HomePageData> onCreateLoader(int id, Bundle args) {
        APIAsyncTaskLoader apiLoader = new APIAsyncTaskLoader(mContext);
        return apiLoader;
    }

    @Override
    public void onLoadFinished(Loader<HomePageData> loader, HomePageData data) {
        if (mApiCallBack != null && !data.hasError()) {
            mApiCallBack.onResponseReceived(data);
        } else if (mApiCallBack != null) {
            mApiCallBack.onError(data.getError());
        }
    }

    @Override
    public void onLoaderReset(Loader<HomePageData> loader) {

    }
}

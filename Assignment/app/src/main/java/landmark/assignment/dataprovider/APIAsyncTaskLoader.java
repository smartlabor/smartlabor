package landmark.assignment.dataprovider;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Iterator;

import landmark.assignment.dataprovider.datamodel.HomePageData;
import landmark.assignment.dataprovider.datamodel.Product;
import landmark.assignment.dataprovider.datamodel.ProductBlockData;
import landmark.assignment.dataprovider.datamodel.ProductData;


/**
 * Created by Binil on 25/09/2016.
 */
public class APIAsyncTaskLoader extends AsyncTaskLoader<HomePageData> {

    private static final String BASE_URL = "http://www.landmarkshops.in/";
    private static final String PRODUCT_MAIN_BLOCK = "[class=block]";
    private static final String PRODUCT_HEAD_MAIN = "col-sm-9";
    private static final String TAG_H2 = "h2";
    private static final String CLASS_PRODUCTS = "products";
    private static final String TAG_IMG = "img";
    private static final String ATTR_SRC = "src";
    private static final String CLASS_TEXT_UPPER = "text-uppercase";
    private static final String ATTR_ITEM_PROP = "itemprop";
    private static final String ATTR_ITEM_PROP_VALUE = "name";
    private static final String CLASS_RIGHT_BOTTOM = "rightbottom";
    private static final String CLASS_LEFT_BOTTOM = "leftbottom";
    private static final String TAG_DEL = "del";
    private static final String ATTR_VAL = "price";

    public APIAsyncTaskLoader(Context context) {
        super(context);
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void deliverResult(HomePageData data) {
        super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        reset();
        forceLoad();
    }


    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
    }

    @Override
    public HomePageData loadInBackground() {
        HomePageData homePageData = new HomePageData();
        try {
            if (!isNetworkAvailable(getContext())) {
                throw new Exception("Please connect to network!!");
            }
            Document doc = Jsoup.connect(BASE_URL).get();
            if (doc != null) {
                Elements landmarkCorosalelements = doc.select(PRODUCT_MAIN_BLOCK);
                if (landmarkCorosalelements != null) {
                    for (int i = 0; i < landmarkCorosalelements.size(); i++) {
                        //create product block data
                        ProductBlockData productBlockData = new ProductBlockData();

                        Element corosalMain = landmarkCorosalelements.get(i);

                        //Area where find the first main title of the product
                        if (corosalMain != null) {
                            Elements colSm9 = corosalMain.getElementsByClass(PRODUCT_HEAD_MAIN);
                            if (colSm9 != null) {
                                if (colSm9.first() != null) {
                                    Elements el2 = colSm9.first().getElementsByTag(TAG_H2);//this has only one node allway on html
                                    //set product title to the main object
                                    productBlockData.setProductTitle(el2.text());
                                }
                            }
                        }

                        //Area where find the first main title of the product
                        Elements products = corosalMain.getElementsByClass(CLASS_PRODUCTS);
                        for (int j = 0; j < products.size(); j++) {
                            //create the product data object

                            Element product = products.get(j);
                            Elements li = product.getElementsByTag("li");
                            //finding the sub product list
                            Iterator<Element> iterator = li.iterator();

                            while (iterator.hasNext()) {
                                ProductData productData = new ProductData();
                                Element subproduct = iterator.next();
                                productData.setProductLeft(getProduct(subproduct));
                                subproduct = iterator.next();
                                productData.setProductRight(getProduct(subproduct));
                                productBlockData.addProductData(productData);
                            }
                            homePageData.setProductBlockData(productBlockData);
                        }//end of product loop

                    }//end main loop
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            homePageData.setError(e);
        }
        return homePageData;
    }

    /**
     * getProduct from element
     *
     * @param subProduct
     * @return
     */
    public Product getProduct(Element subProduct) {

        Product productData = new Product();
        //finding image url
        Elements imageContainer = subProduct.getElementsByTag(TAG_IMG);
        String imgSrc = imageContainer.attr(ATTR_SRC);
        productData.setImageUrl(imgSrc);

        //product brand name
        Elements subBrand = subProduct.getElementsByClass(CLASS_TEXT_UPPER);
        String subBrandName = subBrand.text();
        productData.setTitle(subBrandName);

        //product name
        Elements productLabel = subProduct.getElementsByAttributeValue(ATTR_ITEM_PROP, ATTR_ITEM_PROP_VALUE);
        String productLabelName = productLabel.first().text();
        productData.setProductName(productLabelName);

        //right bottom offer text
        Elements rightBottomOffer = subProduct.getElementsByClass(CLASS_RIGHT_BOTTOM);
        String rightBottomOfferText = rightBottomOffer.text();
        productData.setRightCornerText(rightBottomOfferText);

        //right bottom offer text
        Elements leftbottom = subProduct.getElementsByClass(CLASS_LEFT_BOTTOM);
        String leftbottomText = leftbottom.text();
        productData.setLeftCornerText(leftbottomText);

        //price for the product
        Elements priceOld = subProduct.getElementsByTag(TAG_DEL);
        if (priceOld != null) {
            String priceOldText = priceOld.text();
            productData.setActualPrice(priceOldText);
        }

        Elements pricecurrent = subProduct.getElementsByAttributeValue(ATTR_ITEM_PROP, ATTR_VAL);
        if (pricecurrent != null) {
            String pricecurrentText = pricecurrent.text();
            productData.setCurrentPrice(pricecurrentText);
        }
        return productData;
    }
}

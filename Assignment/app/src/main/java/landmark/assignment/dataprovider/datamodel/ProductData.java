package landmark.assignment.dataprovider.datamodel;

/**
 * Created by Binil on 25/09/2016.
 */
public class ProductData {

    public Product getProductLeft() {
        return mProductLeft;
    }

    public void setProductLeft(Product mProductLeft) {
        this.mProductLeft = mProductLeft;
    }

    public Product getProductRight() {
        return mProductRight;
    }

    public void setProductRight(Product mProductRight) {
        this.mProductRight = mProductRight;
    }

    private Product mProductLeft;
    private Product mProductRight;

}

package landmark.assignment;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import landmark.assignment.carousel.CarouselViewPagerAdapter;
import landmark.assignment.customui.CirclePageIndicator;
import landmark.assignment.dataprovider.datamodel.ProductBlockData;
import landmark.assignment.uiutil.TypeFaceUtil;
import lm.assignment.R;

/**
 * Created by Binil on 24/09/2016.
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private int mPageViewerHeight = 0;
    private ArrayList<ProductBlockData> mProductBlockData;

    public MainAdapter(ArrayList<ProductBlockData> list) {
        mProductBlockData = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //its just a calculation as dynamically take value from phone screen
        if (mPageViewerHeight == 0) {
            mPageViewerHeight = (parent.getWidth() / 2) + parent.getResources().getDimensionPixelOffset(R.dimen.viewpager_delta_height);
        }
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_adapter_cell, null);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductBlockData productBlockData = mProductBlockData.get(position);
        holder.mTitleText.setText(productBlockData.getProductTitle());

        if (holder.mCarouselViewPagerAdapter == null) {
            holder.mCarouselViewPagerAdapter = new CarouselViewPagerAdapter(productBlockData.getProductArray());
            holder.mViewPager.setAdapter(holder.mCarouselViewPagerAdapter);
            holder.mCirclePageIndicator.setViewPager(holder.mViewPager);
        } else {
            holder.mCarouselViewPagerAdapter.updateDataSet(productBlockData.getProductArray());
            holder.mCirclePageIndicator.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return mProductBlockData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTitleText;
        public ViewPager mViewPager;
        public CirclePageIndicator mCirclePageIndicator;
        public CarouselViewPagerAdapter mCarouselViewPagerAdapter;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleText = (TextView) itemView.findViewById(R.id.main_adapter_title_text);
            mViewPager = (ViewPager) itemView.findViewById(R.id.main_adapter_cell_pager);
            mCirclePageIndicator = (CirclePageIndicator) itemView.findViewById(R.id.main_adapter_cell_pager_indicator);
            TypeFaceUtil.setTypeFace(mTitleText, itemView.getResources().getAssets(), TypeFaceUtil.FontType.LATO_BOLD);
            setHeight();
        }

        /**
         * setting the height of the view pager as it used inside a recycle view.
         * as it has only 2 views in screen,so its an adjust calculation to fit on dynamic screen sizes.
         */
        public void setHeight() {
            mViewPager.getLayoutParams().height = mPageViewerHeight;
            mViewPager.requestLayout();
        }
    }

}

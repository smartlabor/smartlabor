package landmark.assignment.uiutil;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by Binil on 24/09/2016.
 */
public class TypeFaceUtil {

    /**
     * the all custom font used in the app
     */
    public enum FontType {
        LATO_BOLD {
            @Override
            public String getPath() {
                return "fonts/Lato-Bold.ttf";
            }
        }, LATO_MEDIUM {
            @Override
            public String getPath() {
                return "fonts/Lato-Medium.ttf";
            }
        }, LATO_THIN {
            @Override
            public String getPath() {
                return "fonts/Lato-Thin.ttf";
            }
        };

        public abstract String getPath();
    }

    /**
     * set the custom font to text view from asset
     *
     * @param textView
     * @param manager
     * @param fontType
     */
    public static void setTypeFace(TextView textView, AssetManager manager, FontType fontType) {
        textView.setTypeface(Typeface.createFromAsset(manager, fontType.getPath()));
    }

}

package landmark.assignment;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import landmark.assignment.dataprovider.ApiCallBack;
import landmark.assignment.dataprovider.DataProvider;
import landmark.assignment.dataprovider.datamodel.HomePageData;
import landmark.assignment.uiutil.TypeFaceUtil;
import lm.assignment.R;

/**
 * main activity ,launcher
 */
public class MainActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView mToolbarCenterAlignedTextView;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_n);
        //init the action bar first
        initActionBar();
        mProgressBar = (ProgressBar) findViewById(R.id.activity_main_progress);
        if (savedInstanceState == null) {
            //attach the fragment for main view
            MainFragment mainFragment = new MainFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.activity_main_content, mainFragment, mainFragment.getFragmentDefaultTag())
                    .commit();
        }

    }


    /**
     * This method is used to initialize action bar.
     */
    private void initActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(mToolbar);
        mToolbarCenterAlignedTextView = (TextView) findViewById(R.id.activity_main_toolbar_text);
        mToolbarCenterAlignedTextView.setText(R.string.activity_main_toolbar_title);
        TypeFaceUtil.setTypeFace(mToolbarCenterAlignedTextView, getAssets(), TypeFaceUtil.FontType.LATO_BOLD);
    }

    public void showProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    public void showError(String errorMessage) {
        Snackbar.make(findViewById(R.id.activity_main_content), errorMessage, Snackbar.LENGTH_LONG).show();
    }
}

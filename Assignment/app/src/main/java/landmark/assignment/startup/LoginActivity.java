package landmark.assignment.startup;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import landmark.assignment.MainActivity;
import landmark.assignment.uiutil.TypeFaceUtil;
import lm.assignment.R;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String APP_ID = "80e4eede56844462ef3cdc721208c31f";

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private GoogleApiClient googleApiClient;

    private Toolbar mToolbar;
    private TextView mToolbarCenterAlignedTextView;
    private ProgressBar mProgressBar;

    private TextView mainTextView;
    private TextView mLocationText;
    private EditText mPhoneNumber;
    private Button mLoginButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startup_activity);

        //init the action bar first
        initActionBar();
        initUI();
        mProgressBar = (ProgressBar) findViewById(R.id.activity_main_progress);
        mProgressBar.setVisibility(View.GONE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }

        showProgress();
        googleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();
    }

    /**
     * This method is used to initialize action bar.
     */
    private void initActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(mToolbar);
        mToolbarCenterAlignedTextView = (TextView) findViewById(R.id.activity_main_toolbar_text);
        mToolbarCenterAlignedTextView.setText(R.string.activity_main_toolbar_title);
        TypeFaceUtil.setTypeFace(mToolbarCenterAlignedTextView, getAssets(), TypeFaceUtil.FontType.LATO_BOLD);
    }

    void initUI() {
        mainTextView = (TextView) findViewById(R.id.startup_title);
        mLocationText = (TextView) findViewById(R.id.startup_location);
        mPhoneNumber = (EditText) findViewById(R.id.startup_phone_et);
        mLoginButton = (Button) findViewById(R.id.startup_login_bt);

        TypeFaceUtil.setTypeFace(mLoginButton, getAssets(), TypeFaceUtil.FontType.LATO_BOLD);

        TypeFaceUtil.setTypeFace(mainTextView, getAssets(), TypeFaceUtil.FontType.LATO_MEDIUM);
        TypeFaceUtil.setTypeFace(mLocationText, getAssets(), TypeFaceUtil.FontType.LATO_MEDIUM);
        TypeFaceUtil.setTypeFace(mPhoneNumber, getAssets(), TypeFaceUtil.FontType.LATO_MEDIUM);
        mLoginButton.setOnClickListener(this);
    }

    public void showProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(LoginActivity.class.getSimpleName(), "Connected to Google Play Services!");
        String _Location = "DUBAI";
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();

            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(lat, lon, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    Address address = listAddresses.get(0);

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Location : \n");
                    stringBuilder.append(address.getAddressLine(0));
                    stringBuilder.append(" / ");
                    stringBuilder.append(address.getAddressLine(1));
                    stringBuilder.append(" / ");
                    stringBuilder.append(address.getCountryCode());
                    _Location = stringBuilder.toString();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mLocationText.setText(_Location);
        hideProgress();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(LoginActivity.class.getSimpleName(), "Can't connect to Google Play Services!");
    }


    @Override
    public void onClick(View v) {
        showProgress();
        mLoginButton.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                hideProgress();
            }
        }, 500);
    }
}
